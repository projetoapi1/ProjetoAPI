# ProjetoAPI



## Getting started

Criei estes testes utilizando a linguagem JAVA com a IDEA Intellij, usando os frameworks RestAssured, Junit, Maven.

Para me basear nos testes usei a documentação contida em: https://sicredi-desafio-qe.readme.io/reference/objetivo

Plano de testes:

Por falta de documentação achei dificil definir como reproduzir o "Response - 401 Unauthorized", eu peguei um token e  salvei ele para ver se expira, mas não tenho certeza após quanto tempo o token expira e se expira. Eu tentei usar um token valido e renomear a ultima letra dele e o retorno foi erro 500, ainda por falta de cocumentação é dificil dizer se é um resultado esperado ou se é um bug.

Fora isso separei os testes por modules: test, users e product.

Test:

Verificar se o método get retorna corretamente para o endpoint /test

Users:

Verificar se o post para o endoint /auth/login retorna corretamente o token com usuários validos

Verificar se o post para o endoint /auth/login retorna erro com usuários inválidos

Verificar se a busca por usuários para o endpoint /users retorna todos os usuários cadastrados

Verificar se a busca por usuários para o endpoint /users retorna os campos usario e senha preenchidos

Verificar se a busca por produtos para o endpoint /auth/products retorna todos os produtos do cliente quando autorizado

Verificar se a busca por produtos para o endpoint /auth/products não retorna todos os produtos do cliente quando sem autorização

Verificar se a busca por produtos para o endpoint /auth/products não retorna todos os produtos do cliente quando autorização está vencida.

Adicionar produto no endpoint /products/add

Verificar se todos se busca por todos os produtos funciona corretamente para o endpoint /products

Verificar se é possível buscar um produto por id pelo endpoint /products/{id}

Verificar se erro é exibido ao buscar um produto por id invalido pelo endpoint /products/{id}



Bugs:

Alguns requests que deveriam retornar 201 retornam 200:

post no endpoint /auth/login

post no endpoint /products/add

Mensagem de erro do endpoint /auth/products quando token está expirado também não está correta.



Melhorias: A documentação é bastante limitada e há pouca informações adicionais como regras de negócio, que poderiam contribuir muito para elaboração de novos casos de testes